import java.util.Arrays;
import java.util.Iterator;


public class CustomArrayList<T> implements Iterable<T> {
    private final static int DEFAULT_CAPACITY = 10;
    private int size;
    private Object[] innerArray;

    public CustomArrayList(){
        innerArray = new Object[DEFAULT_CAPACITY];
    }
    public int size(){
        return size;
    }
    public boolean add(T elementToAdd){
        ensureCapacity();
        innerArray[size] = elementToAdd;
        size = size +1;
        return true;
    }

    private void ensureCapacity(){
        if(size>=innerArray.length-1){
            grow();
        }
    }
    public T get(int index){
        return (T) innerArray[index];
    }
    private void grow(){
        innerArray = Arrays.copyOf(innerArray, innerArray.length + DEFAULT_CAPACITY);
    }

    private void rangeCheck(int index){
        if(index>=size){
            throw new IndexOutOfBoundsException("Attempting to get element out of custom array list size. Current size:" +
                    (size-1) + " . Index: " + index);
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int index =0;

            @Override
            public boolean hasNext() {
                return !(innerArray[index]==null);
            }

            @Override
            public T next() {
                T objToReturn = (T) innerArray[index];
                index++;
                return objToReturn;
            }
        };
    }
}
