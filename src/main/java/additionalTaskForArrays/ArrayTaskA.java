package additionalTaskForArrays;

import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Random;


class ArrayTaskA {
    private static final Logger Log = Logger.getLogger(ArrayTaskA.class);
    private static Random generate = new Random();
    static final int STUB_FOR_IGNORING_COMPARISON;

    static {
        STUB_FOR_IGNORING_COMPARISON = 111;
    }

    private static final int ARRAYS_LENGTH = 10;
    private static final int RANDOM_BOUND = 7;

    public static void main(String[] args) {
        createThirdArrayWithSameValues(removeDuplicatedValues(ArrayTaskA.generateArray()),
                removeDuplicatedValues(ArrayTaskA.generateArray()));
        createThirdArrayWithUniqueValues(removeDuplicatedValues(ArrayTaskA.generateArray()),
                removeDuplicatedValues(ArrayTaskA.generateArray()));
    }

    public static int[] generateArray() {
        int[] generatedArray = new int[ARRAYS_LENGTH];
        for (int i = 0; i < generatedArray.length ; i++) {
            generatedArray[i] = generate.nextInt(RANDOM_BOUND);
        }
        Log.info("Array: " + Arrays.toString(generatedArray));
        return generatedArray;
    }

    private static int[] removeDuplicatedValues(int[] arrayWithUniqueValues) {
        for (int j = 0; j < arrayWithUniqueValues.length; j++) {
            for (int i = j + 1; i < arrayWithUniqueValues.length; i++) {
                if (arrayWithUniqueValues[j] == arrayWithUniqueValues[i]) {
                    arrayWithUniqueValues[i] = STUB_FOR_IGNORING_COMPARISON;
                }
            }
        }
        Log.debug("Array after removing duplicates:" +
                Arrays.toString(arrayWithUniqueValues));
        return arrayWithUniqueValues;
    }

    private static int[] cleanArrayFromRedundantValues(int[] arrayToCleanUp) {
        Log.debug("Array with stubs(" + STUB_FOR_IGNORING_COMPARISON + "): "
                + Arrays.toString(arrayToCleanUp));
        int counterOfValidValues = 0;
        for (int value : arrayToCleanUp) {
            if (value != STUB_FOR_IGNORING_COMPARISON) {
                counterOfValidValues++;
            }
        }
        int[] cleanedArray = new int[counterOfValidValues];
        int index = 0;
        for (int i : arrayToCleanUp) {
            if (i != STUB_FOR_IGNORING_COMPARISON) {
                cleanedArray[index] = i;
                index++;
            }
        }
        return cleanedArray;
    }


    private static void createThirdArrayWithSameValues(int[] firstArray, int[] secondArray) {
        for (int i = 0; i < firstArray.length; i++) {
            boolean found = false;
            if (firstArray[i] != STUB_FOR_IGNORING_COMPARISON) {
                for (int value : secondArray) {
                    if (firstArray[i] == value) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    firstArray[i] = STUB_FOR_IGNORING_COMPARISON;
                }
            }
        }
        Log.info("Third Array generated on the same values from first and second Arrays:"
                + Arrays.toString(cleanArrayFromRedundantValues(firstArray)));
    }

    private static void createThirdArrayWithUniqueValues (int[] firstArray, int[] secondArray){
        for (int i=0; i<firstArray.length;i++){
            for (int j=0; j<secondArray.length;j++ ) {
                if (firstArray[i] == secondArray[j]) {
                    firstArray[i] = STUB_FOR_IGNORING_COMPARISON;
                    secondArray[j]=STUB_FOR_IGNORING_COMPARISON;
                }
            }
        }
        int[] cleanedFirstArray=cleanArrayFromRedundantValues(firstArray);
        int[] cleanedSecondArray=cleanArrayFromRedundantValues(secondArray);
        Log.info(String.format("First array without %s stubs: %s", STUB_FOR_IGNORING_COMPARISON,
                Arrays.toString(cleanedFirstArray)));
        Log.info(String.format("Second array without %s stubs: %s", STUB_FOR_IGNORING_COMPARISON,
                Arrays.toString(cleanedSecondArray)));

        int[]mergedArray = new int[cleanedFirstArray.length + cleanedSecondArray.length];
        int count = 0;

        for(int i = 0; i < cleanedFirstArray.length; i++) {
            mergedArray[i] = cleanedFirstArray[i];
            count++;
        }
        for (int i : cleanedSecondArray) {
            mergedArray[count++] = i;
        }
        Log.info("Third Array generated on the unique values from first and second Arrays:"+
                Arrays.toString(mergedArray));
    }
}
