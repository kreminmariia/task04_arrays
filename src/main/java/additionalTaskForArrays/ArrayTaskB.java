package additionalTaskForArrays;


import org.apache.log4j.Logger;

import java.util.Arrays;

import static additionalTaskForArrays.ArrayTaskA.STUB_FOR_IGNORING_COMPARISON;

public class ArrayTaskB {
    private static final Logger Log = Logger.getLogger(ArrayTaskA.class);

    public static void main(String[] args) {
        ArrayTaskA arrayTaskA = new ArrayTaskA();
        int[] resultOfTaskArrayB = detectDuplicates(ArrayTaskA.generateArray());
    }

    private static int[] detectDuplicates(int[] primaryArray) {
        for (int j = 0; j < primaryArray.length; j++) {
            int counterOfDuplicates = 0;
            int[] indexes = new int[primaryArray.length];
            for (int i = j + 1; i < primaryArray.length; i++) {
                if (primaryArray[j] == primaryArray[i]) {
                    indexes [counterOfDuplicates]=i;
                    counterOfDuplicates++;
                }
            }
            if (counterOfDuplicates >= 2) {
            primaryArray[j] = STUB_FOR_IGNORING_COMPARISON;
                for (int k=0; k<indexes.length;k++) {
                    if (indexes[k]==0 && !(k==0)){
                        continue;
                    }
                    primaryArray[indexes[k]] = STUB_FOR_IGNORING_COMPARISON;
                }
            }
        }
        Log.info("Array after removing duplicates:" +
                Arrays.toString(primaryArray));
        return primaryArray;
    }
}