package country_capital;


public class Pair implements Comparable {

    private String country;
    private String capital;

    public Pair(String country, String capital) {
        this.country = country;
        this.capital = capital;

    }

    public String getCountry() {

        return country;
    }

    public String getCapital() {
        return capital;
    }

    @Override
    public int compareTo(Object o) {
        return country.compareTo(((Pair)o).country);
    }

}
